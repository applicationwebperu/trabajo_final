﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using WebApplication.Models;

namespace WebApplication.ViewModel
{

    public class EstadoIncidenciaViewModel
    {
        public List<Estado_incidencia> ListEstadoIncidencia { get; set; }
        public Estado_incidencia obj { get; set; }

        public int NumeroEstadoIncidencia { get; set; }
        public int NumIncidencias { get; set; }
        public int? EstadoIncidenciaId { get; set; }

        public EstadoIncidenciaViewModel()
        {

        }

        public void CargarDatos()
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            ListEstadoIncidencia = context.Estado_incidencia.Where(x => x.Estado == "ACT").ToList();
            NumeroEstadoIncidencia = ListEstadoIncidencia.Count();
        }

        public void CargarEstadoIncidencia(int? id)
        {
            EstadoIncidenciaId = id;
            if (EstadoIncidenciaId.HasValue)
            {
                trabajo_partialEntities context = new trabajo_partialEntities();
                obj = context.Estado_incidencia.FirstOrDefault(x => x.IdEstado_incidendia == id);
            }
        }

        public int NumeroIncidencias(int id)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            NumIncidencias = context.Estado_incidencia.FirstOrDefault(x => x.IdEstado_incidendia == id).Incidencia.Count();
            return NumIncidencias;
        }

    }
}