﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.ViewModel
{

    public class BitacoraArchivosViewModel
    {
        public List<Archivo> ListBitacoraArchivos { get; set; }
        public Archivo obj { get; set; }
        public int NumeroBitacoraArchivos { get; set; }
        public int? ArchivoId { get; set; }
        public List<Incidencia> ListIncidencia { get; set; }
        public int SelectedIncidencia { get; set; }
        public BitacoraArchivosViewModel()
        {

        }

        public void CargarDatos()
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            ListBitacoraArchivos = context.Archivo.Where(x => x.Estado == "ACT").ToList();
            NumeroBitacoraArchivos = ListBitacoraArchivos.Count();
            ListIncidencia = context.Incidencia.Where(x => x.Estado == "ACT").ToList();
        }

        public void CargarDatos(int id)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            ListBitacoraArchivos = context.Archivo.Where(x => x.Estado == "ACT").Where(x=>x.IdIncidencia == id).ToList();
            NumeroBitacoraArchivos = ListBitacoraArchivos.Count();
            ListIncidencia = context.Incidencia.Where(x => x.Estado == "ACT").Where(x=>x.IdIncidencia == id).ToList();
        }

        public void CargarArchivo(int? id)
        {
            ArchivoId = id;
            if (ArchivoId.HasValue)
            {
                trabajo_partialEntities context = new trabajo_partialEntities();
                obj = context.Archivo.FirstOrDefault(x => x.IdArchivo == id);
            }
        }

        public IEnumerable<SelectListItem> EnumIncidencia()
        {
            return new SelectList(ListIncidencia, "IdIncidencia", "IdIncidencia");
        }

    }
}
