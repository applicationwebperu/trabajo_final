﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using WebApplication.Models;

namespace WebApplication.ViewModel
{

    public class BitacoraSeguimientosViewModel
    {
        public List<Seguimiento> ListBitacoraSeguimientos { get; set; }
        public Seguimiento obj { get; set; }
        public int NumeroBitacoraSeguimientos { get; set; }
        public int? SeguimientoId { get; set; }

        public BitacoraSeguimientosViewModel()
        {

        }

        public void CargarDatos()
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            ListBitacoraSeguimientos = context.Seguimiento.Where(x => x.Estado == "ACT").ToList();
            NumeroBitacoraSeguimientos = ListBitacoraSeguimientos.Count();
        }

        public void CargarSeguimiento(int? id)
        {
            SeguimientoId = id;
            if (SeguimientoId.HasValue)
            {
                trabajo_partialEntities context = new trabajo_partialEntities();
                obj = context.Seguimiento.FirstOrDefault(x => x.IdSeguimiento == id);
            }
        }

        //EDIT AUTO (INCIDENCIA)
        public void editSeguimiento(Incidencia objIncidencia, int id)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            Seguimiento seg = new Seguimiento();
            User user = context.User.FirstOrDefault(x => x.IdUser == id);

            seg.Acronimo = "Edit incidencia #" + objIncidencia.IdIncidencia;
            seg.Estado = "ACT";
            seg.IdIncidencia = objIncidencia.IdIncidencia;
            seg.Fecha = DateTime.Now;
            seg.Descripcion = "Edited By : " + user.IdUser
                + " - Estado set to : " + objIncidencia.IdEstado_Incidencia;
            context.Seguimiento.Add(seg);
            context.SaveChanges();
        }

        //DELETE AUTO (INCIDENCIA)
        public void deleteSeguimiento(Incidencia objIncidencia, int id)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            Seguimiento seg = new Seguimiento();
            User user = context.User.FirstOrDefault(x => x.IdUser == id);

            seg.Acronimo = "Delete incidencia #" + objIncidencia.IdIncidencia;
            seg.Estado = "ACT";
            seg.IdIncidencia = objIncidencia.IdIncidencia ;
            seg.Fecha = DateTime.Now;
            seg.Descripcion = "Deleted By : " + user.Username;
            context.Seguimiento.Add(seg);
            context.SaveChanges();
        }

    }
}