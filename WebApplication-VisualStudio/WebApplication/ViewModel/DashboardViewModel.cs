﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using WebApplication.Models;

namespace WebApplication.ViewModel
{

    public class DashboardViewModel
    {
        public String NombreDirector { get; set; }
        public int NumeroSedes { get; set; }
        public int NumeroUsuarios { get; set; }
        public List<Sed> ListSed { get; set; }
        public Sed obj { get; set; }

        public DashboardViewModel()
        {

        }

        public void CargarDatos()
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            NombreDirector = context.Escuela.Find(1).Director;
            NumeroUsuarios = context.User.Where(x => x.Estado == "ACT").Count();
            ListSed = context.Sed.Where(x => x.Estado == "ACT").ToList();
            NumeroSedes = ListSed.Count();
        }
    }
}
