﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using WebApplication.Models;

namespace WebApplication.ViewModel
{
    public class LoginViewModel
    {
        public String Role { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
        public String PasswordNew { get; set; }
        public String PasswordNewConfirm { get; set; }
        public String Nombre { get; set; }
        public LoginViewModel()
        {

        }

        public String RoleUsuario(int isSupervisor)
        {
            if (isSupervisor == 0)
                Role = "Administrador";
            else if (isSupervisor == 1)
                Role = "Supervisor";
            return Role;
        }

    }
}
