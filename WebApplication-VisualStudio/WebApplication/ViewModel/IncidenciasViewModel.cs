﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.ViewModel
{

    public class IncidenciasViewModel
    {
        public List<Incidencia> ListIncidencias { get; set; }
        public List<Sed> ListSedes { get; set; }
        public List<Tipo_incidencia> ListTipoincidencia { get; set; }
        public List<Estado_incidencia> ListEstadoincidencia { get; set; }
        public int NumeroIncidencias { get; set; }
        public int NumeroArchivos { get; set; }
        public Incidencia obj { get; set; }
        public int? IncidenciaId { get; set; }
        [Display(Name = "Estado Selected")]
        public String SelectedEstadoId { get; set; }
        public String SelectedTipoId { get; set; }
        public String SelectedSedId { get; set; }


        public IncidenciasViewModel()
        {

        }

        public void CargarDatos()
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            ListIncidencias = context.Incidencia.Where(x => x.Estado == "ACT").ToList();
            NumeroIncidencias = ListIncidencias.Count();
            ListEstadoincidencia = context.Estado_incidencia.Where(x => x.Estado == "ACT").ToList();
            ListSedes = context.Sed.Where(x => x.Estado == "ACT").ToList();
            ListTipoincidencia = context.Tipo_incidencia.Where(x => x.Estado == "ACT").ToList();
        }

        public void CargarIncidencia(int? id)
        {
           IncidenciaId = id;
            if (IncidenciaId.HasValue)
            {
                trabajo_partialEntities context = new trabajo_partialEntities();
                obj = context.Incidencia.FirstOrDefault(x => x.IdIncidencia == id);
            }
        }

        public int NumArchivos(int id)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            NumeroArchivos = context.Incidencia.FirstOrDefault(x => x.IdIncidencia == id).Archivo.Where(x => x.Estado == "ACT").Count();
            return NumeroArchivos;
        }

        public IEnumerable<SelectListItem> EnumSedes()
        {
            return new SelectList(ListSedes, "IdSed", "Nombre");
            
        }

        public IEnumerable<SelectListItem> EnumTipoIncidenia()
        {
            return new SelectList(ListTipoincidencia, "IdTipo_incidencia", "Acronimo");
        }

        public IEnumerable<SelectListItem> EnumEstadoIncidencia()
        {
            return new SelectList(ListEstadoincidencia, "IdEstado_incidendia", "Acronimo");
        }

        public int SearchIndexSed(String str)
        {
            int index = 0;
            int i = 1;
            foreach (var item in ListSedes)
            {
                if (str.Equals(ListSedes.ElementAtOrDefault(i)))
                    index = i;
                i++;
            }
            return 0;
        }

    }
}