﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using WebApplication.Models;

namespace WebApplication.ViewModel
{

    public class TipoIncidenciaViewModel
    {
        public List<Tipo_incidencia> ListTipoIncidencia { get; set; }
        public Tipo_incidencia obj { get; set; }

        public int NumeroTipoIncidencia { get; set; }
        public int NumIncidencias { get; set; }
        public int? TipoIncidenciaId { get; set; }


        public TipoIncidenciaViewModel()
        {

        }

        public void CargarDatos()
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            ListTipoIncidencia = context.Tipo_incidencia.Where(x => x.Estado == "ACT").ToList();
            NumeroTipoIncidencia = ListTipoIncidencia.Count();
        }

        public void CargarTipoIncidencia(int? id)
        {
            TipoIncidenciaId = id;
            if (TipoIncidenciaId.HasValue)
            {
                trabajo_partialEntities context = new trabajo_partialEntities();
                obj = context.Tipo_incidencia.FirstOrDefault(x => x.IdTipo_incidencia == id);
            }
        }

        public int NumeroIncidencias(int id)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            NumIncidencias = context.Tipo_incidencia.FirstOrDefault(x => x.IdTipo_incidencia == id).Incidencia.Count();
            return NumIncidencias;
        }

    }
}