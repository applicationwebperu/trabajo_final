﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using WebApplication.Models;

namespace WebApplication.ViewModel
{

    public class SolicitanteViewModel
    {
        public Solicitante obj { get; set; }
        public int? SolicitanteId { get; set; }
        
        public SolicitanteViewModel()
        {
        }

        public void CargarSolicitante(int? id)
        {
            SolicitanteId = id;
            if (SolicitanteId.HasValue)
            {
                trabajo_partialEntities context = new trabajo_partialEntities();
                obj = context.Solicitante.FirstOrDefault(x => x.IdSolicitante == id);
            }
        }

    }
}