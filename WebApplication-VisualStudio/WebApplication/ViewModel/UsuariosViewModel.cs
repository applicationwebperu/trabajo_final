﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.ViewModel
{

    public class UsuariosViewModel
    {
        public List<User> ListUsuarios { get; set; }
        public User obj { get; set; }
        public int NumeroUsuarios { get; set; }
        public String Role { get; set; }
        public int? UsuarioId { get; set; }
        public String AdminType { get; set; }

        public UsuariosViewModel()
        {
        }

        public void CargarDatos()
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            ListUsuarios = context.User.Where(x => x.Estado == "ACT").ToList();
            NumeroUsuarios = ListUsuarios.Count();
        }

        public void CargarUsuario(int? id)
        {
            UsuarioId = id;
            if (UsuarioId.HasValue)
            {
                trabajo_partialEntities context = new trabajo_partialEntities();
                obj = context.User.FirstOrDefault(x => x.IdUser == id);
            }
        }

        public String RoleUsuario(int isSupervisor)
        {
            if (isSupervisor == 0)
                Role = "Administrador";
            else if (isSupervisor == 1)
                Role = "Supervisor";
            return Role;
        }

        public IEnumerable<SelectListItem> EnumAdminType()
        {
            return new SelectList(new[]
        {
            new SelectListItem { Text = "0", Value = "Adminsitrator" },
            new SelectListItem { Text = "1", Value = "Supervisor" },
        }, "Text","Value");
        }

    }
}