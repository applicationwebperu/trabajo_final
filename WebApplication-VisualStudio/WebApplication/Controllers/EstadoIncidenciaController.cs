﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.ViewModel;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class EstadoIncidenciaController : Controller
    {
        //
        // GET: /EstadoIncidencia/
        public ActionResult Index()
        {
            if (Session["objUser"] != null)
            {
                EstadoIncidenciaViewModel objViewModel = new EstadoIncidenciaViewModel();
                objViewModel.CargarDatos();
                return View("EstadoIncidencia", objViewModel);
            }
            return RedirectToAction("Login","Home");
        }


        [HttpPost]
        public ActionResult Add(EstadoIncidenciaViewModel objView)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();

            if (objView == null)
            {
                return Json("error : not found objView");
            }

            objView.obj.Estado = "ACT";
            context.Estado_incidencia.Add(objView.obj);
            context.SaveChanges();

            return Json("ok");
        }

        [HttpPost]
        public ActionResult Edit(EstadoIncidenciaViewModel objView)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            Estado_incidencia original = context.Estado_incidencia.FirstOrDefault(x => x.IdEstado_incidendia == objView.obj.IdEstado_incidendia);
            if (objView == null)
            {
                return Json("error : not found objView");
            }
            if (original == null)
            {
                return Json("error : not found original in database");
            }
            if(original.Acronimo == objView.obj.Acronimo && original.Descripcion == objView.obj.Descripcion)
            {
              return Json("notmodified");
            }
            original.Acronimo = objView.obj.Acronimo;
            original.Descripcion = objView.obj.Descripcion;
            context.SaveChanges();
            return Json("ok");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            Estado_incidencia original = context.Estado_incidencia.FirstOrDefault(x => x.IdEstado_incidendia == id);
            if (original == null)
            {
                return Json("not deleted : cannot find into DB", JsonRequestBehavior.AllowGet);
            }
            original.Estado = "INA";
            context.SaveChanges();

            return Json("ok", JsonRequestBehavior.AllowGet);
        }
	}
}
