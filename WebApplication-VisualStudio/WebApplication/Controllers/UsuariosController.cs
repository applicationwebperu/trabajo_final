﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.ViewModel;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class UsuariosController : Controller
    {
        //
        // GET: /Usuarios/
        public ActionResult Index()
        {
            if (Session["objUser"] != null)
            {
                UsuariosViewModel objViewModel = new UsuariosViewModel();
                objViewModel.CargarDatos();
                return View("Usuarios",objViewModel);
            }
            return RedirectToAction("Login", "Home");
        }


        [HttpPost]
        public ActionResult Add(UsuariosViewModel objView)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();

            if (objView == null)
            {
                return Json("error : not found objView");
            }
            if (objView.AdminType == "1") objView.obj.IsSupervisor = 1;
            else objView.obj.IsSupervisor = 0;
            objView.obj.Estado = "ACT";
            context.User.Add(objView.obj);
            context.SaveChanges();

            return Json("ok");
        }

        [HttpPost]
        public ActionResult Edit(UsuariosViewModel objView)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            User original = context.User.FirstOrDefault(x => x.IdUser == objView.obj.IdUser);
            if (objView == null)
            {
                return Json("error : not found objView");
            }
            if (original == null)
            {
                return Json("error : not found original in database");
            }
            if (original.NombreApe == objView.obj.NombreApe && original.IsSupervisor.ToString() == objView.AdminType)
            {
                return Json("notmodified");
            }
            original.NombreApe = objView.obj.NombreApe;
            if (objView.AdminType == "1") original.IsSupervisor = 1;
            else original.IsSupervisor = 0;
            context.SaveChanges();
            return Json("ok");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            User original = context.User.FirstOrDefault(x => x.IdUser == id);
            if (original == null)
            {
                return Json("not deleted : cannot find into DB", JsonRequestBehavior.AllowGet);
            }
            original.Estado = "INA";
            context.SaveChanges();

            return Json("ok", JsonRequestBehavior.AllowGet);
        }

	}
}