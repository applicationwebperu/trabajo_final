﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.ViewModel;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class IncidenciasController : Controller
    {
        //
        // GET: /Incidencias/
        public ActionResult Index()
        {
            if (Session["objUser"] != null)
            {
                IncidenciasViewModel objViewModel = new IncidenciasViewModel();
                objViewModel.CargarDatos();
                return View("Incidencias",objViewModel);
            }
            return RedirectToAction("Login", "Home");
        }


        [HttpPost]
        public ActionResult Add(IncidenciasViewModel objView)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            BitacoraSeguimientosViewModel objSeguimiento = new BitacoraSeguimientosViewModel();
            Seguimiento seg = new Seguimiento();
            Solicitante sol = new Solicitante();
            User user = ((WebApplication.Models.User)Session["objUser"]);
            String tmp = null;
            int id = 0;

            if (objView == null)
            {
                return Json("error : not found objView");
            }
            using (var dbContextTransaction = context.Database.BeginTransaction())
            {
                try
                {
                    //Add solicitante 
                    sol.NombreApe = objView.obj.Solicitante.NombreApe;
                    sol.Email = objView.obj.Solicitante.Email;
                    sol.Estado = "ACT";
                    context.Solicitante.Add(sol);
                    //context.SaveChanges();
                    //Add incidencia
                    objView.obj.Estado = "ACT";
                    objView.obj.IdRegistrador = user.IdUser;
                        //Set idSed idEstado idTipo Incidencia
                        tmp = objView.SelectedEstadoId;
                        if (Int32.TryParse(tmp, out  id))
                        {
                            objView.obj.IdEstado_Incidencia = id;
                        }else{Console.Write("Error in Parsing IdEdtadoIncidencia");}

                        tmp = objView.SelectedTipoId;
                        if (Int32.TryParse(tmp, out  id))
                        {
                            objView.obj.IdTipo_Incidencia = id;
                        }else{Console.Write("Error in Parsing IdTipoIncidencia");}

                        tmp = objView.SelectedSedId;
                        if (Int32.TryParse(tmp, out  id))
                        {
                            objView.obj.IdSed = id;
                        }else{Console.Write("Error in Parsing IdSed");}
                        objView.obj.Solicitante = sol;
                    //objView.obj.IdSolicitante = context.Solicitante.Last().IdSolicitante;
                    context.Incidencia.Add(objView.obj);
                    //context.SaveChanges();
                    //Add seguimiento
                    //objViewj = context.Incidencia.Last();
                    seg.Acronimo = "INSERT incidencia #" + objView.obj.IdIncidencia;
                    seg.Estado = "ACT";
                    seg.Fecha = DateTime.Now;
                    seg.Descripcion = "Create by:" + user.IdUser + " - Estado: " + context.Estado_incidencia.FirstOrDefault(x => x.IdEstado_incidendia == objView.obj.IdEstado_Incidencia).Acronimo;
                    //seg.IdIncidencia = context.Incidencia.Last().IdIncidencia;
                    seg.Incidencia = objView.obj;
                    context.Seguimiento.Add(seg);
                    context.SaveChanges();
                    dbContextTransaction.Commit();
                }
                catch (Exception)
                {
                    dbContextTransaction.Rollback();
                }
            }
            return Json("ok");
        }

        [HttpPost]
        public ActionResult Edit(IncidenciasViewModel objView)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            Incidencia original = context.Incidencia.FirstOrDefault(x => x.IdIncidencia == objView.obj.IdIncidencia);
            User user = ((WebApplication.Models.User)Session["objUser"]);
            Seguimiento seg = new Seguimiento();
            String tmp = null;
            int idSed = 0;
            int idTipo = 0;
            int idEstado = 0;
            bool EstadoChange = false;

            if (objView == null)
            {
                return Json("error : not found objView");
            }
            if (original == null)
            {
                return Json("error : not found original in database");
            }

            //Try parsing idSed idEstado idTipo Incidencia
            tmp = objView.SelectedEstadoId;
            if (Int32.TryParse(tmp, out  idEstado)) { }
            else { Console.Write("Error in Parsing IdEdtadoIncidencia"); }
            tmp = objView.SelectedTipoId;
            if (Int32.TryParse(tmp, out  idTipo)) { }
            else { Console.Write("Error in Parsing IdTipoIncidencia"); }
            tmp = objView.SelectedSedId;
            if (Int32.TryParse(tmp, out  idSed)) { }
            else { Console.Write("Error in Parsing IdSed"); }

            if (original.Sed.idSed == idSed && original.Solicitante.NombreApe == objView.obj.Solicitante.NombreApe
                 && original.Solicitante.Email == objView.obj.Solicitante.Email && original.Fecha == objView.obj.Fecha
                 && original.Ubicaccion == objView.obj.Ubicaccion && original.IdEstado_Incidencia == idEstado
                 && original.IdTipo_Incidencia == idTipo && original.Descripcion == objView.obj.Descripcion)
            { 
                return Json("notmodified");
            }
            if (original.IdEstado_Incidencia != idEstado)
            { EstadoChange = true; }
            original.IdSed = idSed;
            original.Solicitante.NombreApe = objView.obj.Solicitante.NombreApe;
            original.Solicitante.Email = objView.obj.Solicitante.Email;
            original.Fecha = objView.obj.Fecha;
            original.Ubicaccion = objView.obj.Ubicaccion;
            original.IdEstado_Incidencia = idEstado;
            original.IdTipo_Incidencia = idTipo;
            original.Descripcion = objView.obj.Descripcion;
            //Add seguimiento if estado change
            if(EstadoChange)
            {
                seg.Acronimo = "EDIT incidencia #" + original.IdIncidencia;
                seg.Estado = "ACT";
                seg.Fecha = DateTime.Now;
                seg.Descripcion = "Edit by:" + user.IdUser + " - Estado:" + context.Estado_incidencia.FirstOrDefault(x => x.IdEstado_incidendia == idEstado).Acronimo;
                seg.IdIncidencia = original.IdIncidencia;
                context.Seguimiento.Add(seg);
            }
            context.SaveChanges();
            return Json("ok");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            Incidencia obj = context.Incidencia.FirstOrDefault(x => x.IdIncidencia == id);
            Solicitante sol = context.Solicitante.FirstOrDefault(x => x.IdSolicitante == obj.IdSolicitante);
            User user = ((WebApplication.Models.User)Session["objUser"]);
            Seguimiento seg = new Seguimiento();

            if (obj == null)
            {
                return Json("not deleted : cannot find into DB", JsonRequestBehavior.AllowGet);
            }
            sol.Estado = "INA";
            obj.Estado = "INA";
            //Add seguimiento
            seg.Acronimo = "DELETE incidencia #" + obj.IdIncidencia;
            seg.Estado = "ACT";
            seg.Fecha = DateTime.Now;
            seg.Descripcion = "Deleted by: " + user.Username + " - Estado : " + obj.Estado_incidencia.Acronimo;
            seg.IdIncidencia = obj.IdIncidencia;
            context.Seguimiento.Add(seg);
            context.SaveChanges();
            return Json("ok", JsonRequestBehavior.AllowGet);
        }

	}
}