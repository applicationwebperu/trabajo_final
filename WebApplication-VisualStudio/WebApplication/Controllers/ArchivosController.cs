﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.ViewModel;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class ArchivosController : Controller
    {
        //
        // GET: /Archvios/
        public ActionResult Index(int? id)
        {
            if (Session["objUser"] != null)
            {
                BitacoraArchivosViewModel objViewModel = new BitacoraArchivosViewModel();
                if (id.HasValue) objViewModel.CargarDatos(id.Value);
                else objViewModel.CargarDatos();
                return View("BitacoraArchivos", objViewModel);
            }
            return RedirectToAction("Login", "Home");
        }


        [HttpPost]
        public ActionResult Add(BitacoraArchivosViewModel objView)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            HttpPostedFileBase file = Request.Files[0];
            if (objView == null)
            {
                return View();
            }
            string name = file.FileName;
            string ext = System.IO.Path.GetExtension(name);
            objView.obj.Extension = ext;
            objView.obj.IdIncidencia = objView.SelectedIncidencia;
            objView.obj.Estado = "ACT";
            //TODO mettre dans l'objet
            System.IO.MemoryStream target = new System.IO.MemoryStream();
            file.InputStream.CopyTo(target);
            objView.obj.Archivo1 = target.ToArray();
            context.Archivo.Add(objView.obj);
            context.SaveChanges();

            return Json("ok");
        }

        [HttpPost]
        public ActionResult Edit(BitacoraArchivosViewModel objView)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            Archivo original = context.Archivo.FirstOrDefault(x => x.IdArchivo == objView.obj.IdArchivo);
            if (objView == null)
            {
                return Json("error : not found objView");
            }
            if (original == null)
            {
                return Json("error : not found original in database");
            }
            if (original.Nombre == objView.obj.Nombre && original.IdIncidencia == objView.SelectedIncidencia)
            {
                return Json("notmodified");
            }
            original.Nombre = objView.obj.Nombre;
            original.IdIncidencia = objView.SelectedIncidencia;
            context.SaveChanges();
            return Json("ok");
        }
        [HttpGet]
        public ActionResult Delete(int id)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            Archivo original = context.Archivo.FirstOrDefault(x => x.IdArchivo == id);
            if (original == null)
            {
                return Json("not deleted : cannot find into DB", JsonRequestBehavior.AllowGet);
            }
            original.Estado = "INA";
            context.SaveChanges();

            return Json("ok", JsonRequestBehavior.AllowGet);
        }
	}
}
