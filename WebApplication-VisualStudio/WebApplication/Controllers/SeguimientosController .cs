﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.ViewModel;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class SeguimientosController : Controller
    {
        //
        // GET: /Seguimientos/
        public ActionResult Index()
        {
            if (Session["objUser"] != null)
            {
                BitacoraSeguimientosViewModel objViewModel = new BitacoraSeguimientosViewModel();
                objViewModel.CargarDatos();
                return View("BitacoraSeguimientos", objViewModel);
            }
            return RedirectToAction("Login", "Home");
        }

	}
}