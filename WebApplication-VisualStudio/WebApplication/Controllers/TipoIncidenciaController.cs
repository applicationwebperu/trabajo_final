﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.ViewModel;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class TipoIncidenciaController : Controller
    {
        //
        // GET: /TipoIncidencia/
        public ActionResult Index()
        {
            if (Session["objUser"] != null)
            {
                TipoIncidenciaViewModel objViewModel = new TipoIncidenciaViewModel();
                objViewModel.CargarDatos();
                return View("TipoIncidencia",objViewModel);
            }
            return RedirectToAction("Login", "Home");
        }


        [HttpPost]
        public ActionResult Add(TipoIncidenciaViewModel objView)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();

            if (objView == null)
            {
                return Json("error : not found objView");
            }

            objView.obj.Estado = "ACT";
            context.Tipo_incidencia.Add(objView.obj);
            context.SaveChanges();

            return Json("ok");
        }

        [HttpPost]
        public ActionResult Edit(TipoIncidenciaViewModel objView)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            Tipo_incidencia original = context.Tipo_incidencia.FirstOrDefault(x => x.IdTipo_incidencia == objView.obj.IdTipo_incidencia);
            if (objView == null) 
            {
                return Json("error : not found objView");
            }
            if (original == null)
            {
                return Json("error : not found original in database");
            }
            if(original.Acronimo == objView.obj.Acronimo && original.Descripcion == objView.obj.Descripcion)
            {
              return Json("notmodified");
            }
            original.Acronimo = objView.obj.Acronimo;
            original.Descripcion = objView.obj.Descripcion;
            context.SaveChanges();
            return Json("ok");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            Tipo_incidencia original = context.Tipo_incidencia.FirstOrDefault(x => x.IdTipo_incidencia == id);
            if (original == null)
            {
                return Json("not deleted : cannot find into DB", JsonRequestBehavior.AllowGet);
            }
            original.Estado = "INA";
            context.SaveChanges();

            return Json("ok", JsonRequestBehavior.AllowGet);
        }

	}
}
