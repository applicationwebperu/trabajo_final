﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.ViewModel;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Inicio()
        {
            if(Session["objUser"] != null)
            {
                DashboardViewModel objViewModel = new DashboardViewModel();
                objViewModel.CargarDatos();
                return View(objViewModel);
            }

            return RedirectToAction("Login");

        }

        public ActionResult Perfil()
        {
            if (Session["objUser"] != null)
            {
                LoginViewModel objViewModel = new LoginViewModel();
                return View(objViewModel);
            }
            return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult EditUser(LoginViewModel objView)
        {
            if (Session["objUser"] != null)
            {
                User usrS = ((WebApplication.Models.User)Session["objUser"]);
                trabajo_partialEntities context = new trabajo_partialEntities();
                User objUser = context.User.FirstOrDefault(x =>
                    x.Password == usrS.Password &&
                    x.Username == usrS.Username);
                if (objView.Password != null && objView.PasswordNewConfirm != null && objView.Password.Length > 0 && objView.Password == objView.PasswordNewConfirm)
                {
                  objUser.Password = objView.Password;
                }
                if (objView.Nombre!= null && objView.Nombre.Length > 3)
                {
                  objUser.NombreApe = objView.Nombre;
                }
                context.SaveChanges();
                Session["objUser"] = objUser;
                return RedirectToAction("Perfil");
            }
            return RedirectToAction("Perfil");
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel objViewModel)
        {
            Console.Write(objViewModel.Password + " " + objViewModel.Username);
            trabajo_partialEntities context = new trabajo_partialEntities();
            User objUser = context.User.FirstOrDefault(x =>
                x.Password == objViewModel.Password &&
                x.Username == objViewModel.Username);
            Console.Write(objUser.Password + " " + objUser.Username);
            if (objUser == null)
            {
                return View(objViewModel);
            }

            Session["objUser"] = objUser;
            return RedirectToAction("Inicio");
        }

        public ActionResult CerrarSesion()
        {
            Session.Clear();
            return RedirectToAction("About");
        }

        [HttpPost]
        public ActionResult AddSed(DashboardViewModel objView){
          trabajo_partialEntities context = new trabajo_partialEntities();

          if (objView == null)
          {
              return Json("error : not found objView");
          }

          objView.obj.Estado = "ACT";
          objView.obj.IdEscuela = 1;
          context.Sed.Add(objView.obj);
          context.SaveChanges();

          return Json("ok");
        }

        [HttpPost]
        public ActionResult EditSed(DashboardViewModel objView){
          trabajo_partialEntities context = new trabajo_partialEntities();
          Sed original = context.Sed.FirstOrDefault(x => x.idSed == objView.obj.idSed);
          if (objView == null)
          {
              return Json("error : not found objView");
          }
          if (original == null)
          {
              return Json("error : not found original in database");
          }
          if(original.Nombre == objView.obj.Nombre && original.Direccion == objView.obj.Direccion)
          {
            return Json("notmodified");
          }
          original.Nombre = objView.obj.Nombre;
          original.Direccion = objView.obj.Direccion;
          context.SaveChanges();
          
          return Json("ok");
        }

        [HttpGet]
        public ActionResult DeleteSed(int id)
        {
            trabajo_partialEntities context = new trabajo_partialEntities();
            Sed original = context.Sed.FirstOrDefault(x => x.idSed == id);
            if (original == null)
            {
                return Json("not deleted : cannot find into DB", JsonRequestBehavior.AllowGet);
            }
            original.Estado = "INA";
            context.SaveChanges();

            return Json("ok", JsonRequestBehavior.AllowGet);
        }
    }
}
